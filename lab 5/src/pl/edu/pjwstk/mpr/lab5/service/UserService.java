package pl.edu.pjwstk.mpr.lab5.service;

import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Collections;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
		List<User> result = new ArrayList<User>();
		
		for(User user: users){
			if(user.getPersonDetails() != null)
			if(user.getPersonDetails().getAddresses().size()>1)
				result.add(user);
		}
		
		return result;
    }

    public static Person findOldestPerson(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
    	User oldest = users.stream()
    	        .max(Comparator.comparing(user -> user.getPersonDetails().getAge()))
    	        .get();
		return oldest.getPersonDetails();
        
    }

    public static User findUserWithLongestUsername(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
    	User longest = users.stream()
    	        .max(Comparator.comparing(user -> user.getName().length()))
    	        .get();
		return longest;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
    	return users.stream()
		.filter(user -> user.getPersonDetails().getAge() > 18)
		.map(user -> user.getPersonDetails().getName() + " " +
			user.getPersonDetails().getSurname()) 
					
		.collect(Collectors.joining(","));
    }
 	
    

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
    	  	

    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
    	Map<Role, List<User>> groupUsersByRole = users.stream()
				.collect(Collectors.groupingBy(user-> user.getPersonDetails().getRole()));
		System.out.println(groupUsersByRole);
		return groupUsersByRole;
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
    	Map<Boolean, List<User>> partitionUsersByAge = users.stream()
				.collect(Collectors.partitioningBy(user -> user.getPersonDetails().getAge() >= 18));
    	System.out.println(partitionUsersByAge);
		return partitionUsersByAge;
    }
}
