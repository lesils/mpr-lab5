import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;
import pl.edu.pjwstk.mpr.lab5.service.UserService;


public class Main {
	private static List<User> users;

	private static void init() {
		
		Person kamil = new Person();
		Person maciek = new Person();
		Person stefan = new Person();
		Person andrzej = new Person();
		Person agnieszka = new Person();
		
		kamil.setAge(21);
		kamil.setName("Kamil");
		kamil.setSurname("Sowak");
		
		maciek.setAge(20);
		maciek.setName("Maciek");
		maciek.setSurname("Konkol");
		
		stefan.setAge(82);
		stefan.setName("Stefan");
		stefan.setSurname("Satory");
		
		andrzej.setAge(24);
		andrzej.setName("Andrzej");
		andrzej.setSurname("Janusz");
		
		agnieszka.setAge(22);
		agnieszka.setName("Agnieszka");
		agnieszka.setSurname("Nowak");
		
		User user1 = new User("saviola", "passwd", kamil);
		User user2 = new User("lesils", "passwd1", maciek);
		User user3 = new User("stefan", "pass123", stefan);
		User user4 = new User("endrju", "pass123", andrzej);
		User user5 = new User("aga", "pass123", agnieszka);
		
		users = Arrays.asList(user1, user2, user3,user4,user5);
		
		Role roleUser = new Role();
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		stefan.setRole(roleAdmin);
		kamil.setRole(roleGuest);
		agnieszka.setRole(roleUser);
		andrzej.setRole(roleGuest);
		maciek.setRole(roleAdmin);
		
		Permission permWrite = new Permission();
		Permission permRead = new Permission();
		Permission permExecute = new Permission();
		
		permWrite.setName("write");
		permRead.setName("read");
		permExecute.setName("execute");
		
		List<Permission> permUser = Arrays.asList(permWrite,permRead);
		List<Permission> permGuest = Arrays.asList(permRead);
		List<Permission> permAdmin = Arrays.asList(permWrite,permRead,permExecute);
		
		roleUser.setPermissions(permUser);
		roleGuest.setPermissions(permGuest);
		roleAdmin.setPermissions(permAdmin);
		
		Person oldest = UserService.findOldestPerson(users);
		System.out.println(oldest.getName());
		System.out.println(oldest.getSurname());
		System.out.println(oldest.getAge());
		
		User longest = UserService.findUserWithLongestUsername(users);
		System.out.println(longest.getName());
		
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		System.out.println(result);
		
		List<String> nameStartingWithA = UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users);
		System.out.println(nameStartingWithA);
		
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
		
		UserService.groupUsersByRole(users);
		UserService.partitionUserByUnderAndOver18(users);
				
	}
	


	public static void main(String[] args) {
		init();
	
	}
}
