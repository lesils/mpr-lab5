package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.mpr.lab5.domain.Address;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.User;
import pl.edu.pjwstk.mpr.lab5.service.UserService;

public class UserSeviceTest {

	@Test
	public void testFindUsersWhoHaveMoreThanOneAddress() {
		User u = new User();
		User userWith2Addresses = new User();
		Person p = new Person();
		Person p2 = new Person();
		Address a1 = new Address("Poland", "Gdynia", "kameliowa", 127);
		Address a2 = new Address("Poland", "torun", "fajna", 15);
		Address a3 = new Address("Poland", "Gdynia", "kwiatkowskiego", 27);
		p.getAddresses().add(a1);
		p2.getAddresses().add(a3);
		p2.getAddresses().add(a2);
		u.setPersonDetails(p);
		userWith2Addresses.setPersonDetails(p2);
		
		List<User> users = new ArrayList<User>();
		users.add(u);
		users.add(userWith2Addresses);

		List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);
		
		assertTrue(result.size()==1);
		assertSame(result.get(0), userWith2Addresses); 
		fail("ups!");
	}

	@Test
	public void testFindOldestPerson() {
		List<User> users;

		Person Romek = new Person();
		Person  Ola= new Person();
		Person tomektomek = new Person();
		Romek.setAge(21);
		tomek.setAge(20);
		ola.setAge(82);
		tomektomek.setName("tomektomek");
		
		
		User user1 = new User("romcio", "pasdasswd", Romek);
		User user2 = new User("emanuel", "passfswd12", Ola);
		User user3 = new User("zenono", "pasdss12322", tomek);

		users = Arrays.asList(user1, user2, user3);
		
		Person result = UserService.findOldestPerson(users);
		assertSame(82, result.getAge());
		
	}

	@Test
	public void testFindUserWithLongestUsername() {
		List<User> users;

		Person romek = new Person();
		Person tomek = new Person();
		Person stefan = new Person();
		romek.setAge(21);
		tomek.setAge(20);
		stefan.setAge(82);
		stefan.setName("Stefan");
		stefan.setSurname("cichy");
		
		User user1 = new User("trawnik", "passwd", romek);
		User user2 = new User("pifos", "passwd1", tomek);
		User user3 = new User("stefan", "pass123", stefan);

		users = Arrays.asList(user1, user2, user3);
		User result = UserService.findUserWithLongestUsername(users);
		assertSame("trawnik", result.getName());
	}

	@Test
	public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() {
		
		List<User> users;
		
		Person romek = new Person();
		Person tomek = new Person();
		Person stefan = new Person();
		
		romek.setAge(21);
		romek.setName("romek");
		romek.setSurname("buc");
		
		tomek.setAge(20);
		tomek.setName("tomek");
		tomek.setSurname("nowak");
		
		
		User user1 = new User("trawnik", "passwd", romek);
		User user2 = new User("pifos", "passwd1", tomek);
		User user3 = new User("stefan", "pass123", stefan);
		
		users = Arrays.asList(user1, user2, user3);
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		
		assertSame("romek,tomek nowak,Stefan cichy", result);
	}
	

	@Test
	public void testGetSortedPermissionsOfUsersWithNameStartingWithA() {
		List<User> users;
		
		Person romek = new Person();
		Person tomek = new Person();
		Person andrzej = new Person();
		
		romek.setAge(21);
		romek.setName("romek");
		romek.setSurname("buc");
		
		tomek.setAge(20);
		tomek.setName("tomek");
		tomek.setSurname("nowak");
		
		andrzej.setAge(82);
		andrzej.setName("Andrzej");
		andrzej.setSurname("cichy");
		
		User user1 = new User("trawnik", "p22asswd", romek);
		User user2 = new User("pifos", "pa3123sswd1", tomek);
		User user3 = new User("stefan", "pa123ss123", andrzej);
		
		users = Arrays.asList(user1, user2, user3);
	}

	
	}
public void testFindOldestPerson() {
	List<User> users;

	Person Romek = new Person();
	Person  Ola= new Person();
	Person tomektomek = new Person();
	Romek.setAge(21);
	tomek.setAge(20);
	ola.setAge(82);
	tomektomek.setName("tomektomek");
	
	
	User user1 = new User("romcio", "pasdasswd", Romek);
	User user2 = new User("emanuel", "passfswd12", Ola);
	User user3 = new User("zenono", "pasdss12322", tomek);

	users = Arrays.asList(user1, user2, user3);
	
	Person result = UserService.findOldestPerson(users);
	assertSame(82, result.getAge());
	
}
}

}
